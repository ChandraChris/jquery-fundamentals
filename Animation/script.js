$(document).ready(function () {
	/*
	   1. show()
	   2. hide()
	   3. toggle()
	   4. fadeIn()
	   5. fadeOut()
	   6. fadeTo()
	   7. slideDown()
	   8. slideUp()
	   9. slideToggle()
	*/
	$('#btn1').click(function () {
		/* body... */
		// $('#box1').show();
		// $('#box1').fadeIn(2000);
		$('#box1').slideDown(2000, function () {
			/* body... */
			alert('Selesai!');
		});
	});
	$('#btn2').click(function () {
		/* body... */
		// $('#box1').hide();
		// $('#box1').fadeOut(2000);
		$('#box1').slideUp(2000).fadeIn(2000).fadeTo(2000, 0.5).fadeTo(2000, 1, function () {
			/* body... */
			alert('Callback Done');
		});
	});
	$('#btn3').click(function () {
		/* body... */
		// $('#box1').toggle();
		// $('#box1').fadeTo(2000, 0.5);
		$('#box1').slideToggle(2000);
	});
	$('#btn4').click(function () {
		/* body... */
		$('#box1').stop();
	});
	$('a').click(function () {
		event.preventDefault();
		$(this).hide('slow');
	});
	$('p').one('click', firstClick);

	function firstClick() {
		$('p').text('You click me first time');
		$(this).click(function () {
			$('p').text('You can not click me anymore');
		})
	}
	/*stop animasi - chaining - callback*/
})