$(document).ready(function () {
	/* body... */
	/*
		1. children -> Untuk satu tag dibawahnya
		2. find -> Mencari Keseluruhan
		3. parent -> Untuk satu tag diatasnya
		4. parents -> seperti dengan >=
		5. parentsUntil -> Seperti dengan >
	*/
	$('div').find('li').css('color', 'red');
	$('.css').parentsUntil('#div1').css('backgroundColor', 'yellow');
});