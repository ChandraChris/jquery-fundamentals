$(document).ready(function () {
	/* body... */
	// var li = document.getElementsByTagName('li');
	var input = document.getElementById('inputName');
	input.placeholder = "Ketikkan teks disini!";

	/* 1. append
	   2. prepend
	   3. after
	   4. before
	*/
	$('ul').prepend('<li>Souma</li>');
	$('ul').append('<li>Shokugeki</li>');
	/* 1. empty
	   2. remove
	*/
	// $('ul').empty();
	// for (var i = 0; i < li.length; i++){	
	// 	li[i].className = "boxes";
	// }
	$('li').addClass('boxes');
	$('h1').css('color', 'yellow');
	$('h1').css('backgroundColor', 'green');
	$('.boxes').css('color', 'blue');
	$('.boxes:first').css('color', 'red');
	$('.boxes:last').css('color', 'green');
	$('.boxes:eq(2)').css('color', 'yellow');
	$('.boxes:eq(3)').css('color', 'orange');

	function click1() {
		// body... 
		$('h1').css({
			'color': 'black',
			'fontSize': '40px'
		});
		// $('ul').append('<li>Shokugeki</li>');

	};
	$('h1').click(function () {
		/* body... */
		click1();
	});
	$('h1').mouseenter(function () {
		/* body... */
		$('h1').css('color', 'lightblue');
	});
	$('h1').mouseleave(function () {
		/* body... */
		$('h1').css('color', 'orange');
	});
	$('.boxes').mouseenter(function () {
		/* body... */
		$(this).css('color', 'lightgreen');
	});
	$('.boxes').mouseleave(function () {
		/* body... */
		$(this).css('color', 'purple');
	});
	$('form').submit(function () {
		/* body... */
		var tulisan = ($('#inputName').val());
		$('h1').text(tulisan);
		event.preventDefault();
	});
	$('button').on({
		"click": function () {
			$('.boxes').css('color', 'black');
		},
		"mouseenter": function () {
			$('.boxes').css('color', 'red');
		}
	})
	/* 1. addClass()
	   2. removeClass()
	   3. toggleClass()
	*/
	$('#Shoku').click(function () {
		/* body... */
		$('#Shoku').toggleClass('change');
	});

	/* Dimensi
		1. width()
		2. height()
		3. innerWidth()
		4. innerHeight()
		5. outerWidth()-> Jika parameter nya berisi true maka dia akan mengambil margin nya juga jika tidak maka sebaliknya
		6. outerHeight()-> Jika parameter nya berisi true maka dia akan mengambil margin nya juga jika tidak maka sebaliknya
	*/
	var nilai = $('#box').width(200).height(200);
	// console.log('Width = ' + nilai);
});