$(document).ready(function () {
    var passwordField = $('#password');
    var button = $('#show_password');
    var usernameField = $('#username');
    $('#show_password').click(function () {
        var passwordFieldType = passwordField.attr('type');
        if (passwordFieldType == 'password') {
            passwordField.attr('type', 'text');
            button.attr('class', 'btn btn-success mb-2 mr-sm-2');
            $(this).text('Hide');
        } else {
            passwordField.attr('type', 'password');
            button.attr('class', 'btn btn-info mb-2 mr-sm-2');
            $(this).text('Show');
        }
    });
    $('#submit').click(function () {
        if ((usernameField.val() === "") || (passwordField.val() === "")) {
            alert('Isi Username dan Password dengan benar!');
        } else {
            alert('Thanks to Registration');
            event.preventDefault();
        }
    })
});