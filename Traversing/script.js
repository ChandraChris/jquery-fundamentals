$(document).ready(function () {
	/* body... */
	$('li').css('color', 'orange');
	$('li').first().css('color', 'red');
	$('li').last().css('color', 'blue');
	$('li').eq(2).css('color', 'green');
	$('li').eq(2).addClass('css');
	$('li').eq(2).addClass('php');
	$('li').filter('.css').css('fontSize', '25px');
	$('li').not('.css').css('backgroundColor', 'lightblue');
})