$(document).ready(function (params) {
    var foo = function (event) {
        if (event) {
            console.log(event);
        } else {
            console.log("this didn't come from a event!");
        }
    };
    $('p').on('click', foo);
    foo();
})